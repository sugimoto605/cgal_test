#include <iostream>
#include <fstream>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Triangulation_3.h>
//
#include <CGAL/Exact_integer.h>
#include <CGAL/Extended_homogeneous.h>
#include <CGAL/Nef_polyhedron_3.h>

void step1_point(){
	using Kernel=CGAL::Simple_cartesian<double>;
	using Point_2=Kernel::Point_2;
	using Segment_2=Kernel::Segment_2;
	using Point_3=Kernel::Point_3;
	//座標点を定義できる
	Point_2 p(1,1), q(10,10);
	std::cout << "p = " << p << std::endl;
	std::cout << "q = " << q.x() << " " << q.y() << std::endl;
	std::cout << "sqdist(p,q) = " << CGAL::squared_distance(p,q) << std::endl;
	Point_3 p3(1,1,1),q3(5,5,5);
	std::cout << "p3 = " << p3 << std::endl;
	std::cout << "q3 = " << q3 << std::endl;
	std::cout << "sqdist(p3,q3) = " << CGAL::squared_distance(p3,q3) << std::endl;
	//線分の概念もある
	Segment_2 s(p,q);
	Point_2 m(12, 11);
	std::cout << "m = " << m << std::endl;
	//有限線分と点の距離を計算
	std::cout << "sqdist(Segment_2(p,q), m) = " << CGAL::squared_distance(s,m) << std::endl;
	std::cout << "p, q, and m ";
	switch (CGAL::orientation(p,q,m)){
		case CGAL::COLLINEAR:
			std::cout << "are collinear\n";
			break;
		case CGAL::LEFT_TURN:
			std::cout << "make a left turn\n";
			break;
		case CGAL::RIGHT_TURN:
			std::cout << "make a right turn\n";
			break;
	}
	std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;
}
void step2_tetra(){
	using Kernel=CGAL::Simple_cartesian<double>;
	//三角錐を作る
	Kernel::Point_3 p( 1.0, 0.0, 0.0);
	Kernel::Point_3 q( 0.0, 0.0, 1.0);
	Kernel::Point_3 r( 0.0, 0.0, 0.0);
	Kernel::Point_3 s( 0.0, 1.0, 0.0);
	CGAL::Polyhedron_3<Kernel> P;
	auto halfedge=P.make_tetrahedron( p, q, r, s);
	//登録した順にイテレータが使える
	auto print_vertex=[](const CGAL::Polyhedron_3<Kernel>& P){
		int n=0;
		for (auto v = P.vertices_begin(); v != P.vertices_end(); ++v) std::cout << "Vertex " << ++n << " = " << v->point() << std::endl;
	};
	print_vertex(P);
	//多面体の辺をリストアップ
	auto print_edge=[](CGAL::Polyhedron_3<Kernel>::Halfedge_handle& H){
		std::cout << H->opposite()->vertex()->point() << " --> " << H->vertex()->point() << std::endl;
	};
	auto print_edges=[](CGAL::Polyhedron_3<Kernel>& P){
		auto print_edge_it=[](CGAL::Polyhedron_3<Kernel>::Halfedge_iterator H){
			std::cout << H->opposite()->vertex()->point() << " --> " << H->vertex()->point() << std::endl;
		};
		int n=0;
		for (auto h = P.halfedges_begin(); h != P.halfedges_end(); ++h) { std::cout << ++n << "th halfedge ";print_edge_it(h);}
	};
	print_edges(P);
	//多面体の面をリストアップ
	auto print_facets=[](CGAL::Polyhedron_3<Kernel>& P){
		auto triangle_normal=[](const CGAL::Polyhedron_3<Kernel>::Facet& f){
			return Kernel::Construct_normal_3()(f.halfedge()->vertex()->point(),f.halfedge()->next()->vertex()->point(),f.halfedge()->opposite()->vertex()->point() );
		};
		auto triangle_area=[](const CGAL::Polyhedron_3<Kernel>::Facet& f){
			return Kernel::Compute_area_3()(f.halfedge()->vertex()->point(),f.halfedge()->next()->vertex()->point(),f.halfedge()->opposite()->vertex()->point() );
		};
		int n=0;
		for (auto f = P.facets_begin(); f != P.facets_end(); ++f) {
			n++;
			std::cout << "Facet " << n;
			if (f->is_triangle()) std::cout << " is triangle area normal to " << triangle_normal(*f) << " with area= " << triangle_area(*f);
			else if (f->is_quad()) std::cout << " is quadrature";
			else std::cout << " is polygon";
			std::cout << std::endl;
		};
	};
	print_facets(P);
	//三角錐なら, 体積も計算できます(p0-p1-p2平面に対しp3の位置によって正負があります)
	std::vector<Kernel::Point_3> ARRY;
	for (auto v = P.vertices_begin(); v != P.vertices_end(); ++v) ARRY.push_back(v->point());
	CGAL::Tetrahedron_3<Kernel> T(ARRY[0],ARRY[1],ARRY[2],ARRY[3]);
	std::cout << "Volume= " << std::abs(T.volume()) << std::endl;
	//辺を分割することもできる
	std::cout << "HALFEDGE分割" << std::endl;
	auto h=halfedge;
	auto g=h->next()->opposite()->next();
	P.split_edge(h->next())->vertex()->point()=Kernel::Point_3(1,0,1);
	P.split_edge(g->next())->vertex()->point()=Kernel::Point_3(0,1,1);
	P.split_edge(g)->vertex()->point()=Kernel::Point_3(1,1,0);
	print_vertex(P);
	print_facets(P);
	//面も分割できる
	auto f=P.split_facet(g->next(),g->next()->next()->next());
	auto e=P.split_edge(f);
	print_vertex(P);
	e->vertex()->point()=Kernel::Point_3(1,1,1);;
	//もう一回だ
	P.split_facet(e,f->next()->next());
	std::cout << "立方体が完成" << std::endl;
	print_vertex(P);
	print_facets(P);
}
//----------------------------------------------
CGAL::Polyhedron_3<CGAL::Simple_cartesian<double>> make_cube(double (&from)[3],double (&width)[3]){
	using Kernel=CGAL::Simple_cartesian<double>;
	Kernel::Point_3 p(from[0]+width[0],from[1],from[2]);
	Kernel::Point_3 q(from[0],from[1],from[2]+width[2]);
	Kernel::Point_3 r(from[0],from[1],from[2]);
	Kernel::Point_3 s(from[0],from[1]+width[1],from[2]);
	CGAL::Polyhedron_3<Kernel> P;
	auto h=P.make_tetrahedron( p, q, r, s);
	auto g=h->next()->opposite()->next();
	P.split_edge(h->next())->vertex()->point()=Kernel::Point_3(from[0]+width[0],from[1],from[2]+width[2]);
	P.split_edge(g->next())->vertex()->point()=Kernel::Point_3(from[0],from[1]+width[1],from[2]+width[2]);
	P.split_edge(g)->vertex()->point()=Kernel::Point_3(from[0]+width[0],from[1]+width[1],from[2]);
	auto f=P.split_facet(g->next(),g->next()->next()->next());
	auto e=P.split_edge(f);
	e->vertex()->point()=Kernel::Point_3(from[0]+width[0],from[1]+width[1],from[2]+width[2]);
	P.split_facet(e,f->next()->next());
	return P;
};
void print_vertex(const CGAL::Polyhedron_3<CGAL::Simple_cartesian<double>>& P){
	int n=0;
	for (auto v = P.vertices_begin(); v != P.vertices_end(); ++v) std::cout << "Vertex " << ++n << " = " << v->point() << std::endl;
};
void print_facet(const CGAL::Polyhedron_3<CGAL::Simple_cartesian<double>>& P){
	using Kernel=CGAL::Simple_cartesian<double>;
	auto triangle_normal=[](const CGAL::Polyhedron_3<Kernel>::Facet& f){
		return Kernel::Construct_normal_3()(f.halfedge()->vertex()->point(),f.halfedge()->next()->vertex()->point(),f.halfedge()->opposite()->vertex()->point() );
	};
	auto triangle_area=[](const CGAL::Polyhedron_3<Kernel>::Facet& f){
		return Kernel::Compute_area_3()(f.halfedge()->vertex()->point(),f.halfedge()->next()->vertex()->point(),f.halfedge()->opposite()->vertex()->point() );
	};
	int n=0;
	for (auto f = P.facets_begin(); f != P.facets_end(); ++f) {
		n++;
		std::cout << "Facet " << n;
		if (f->is_triangle()) std::cout << " is triangle area normal to " << triangle_normal(*f) << " with area= " << triangle_area(*f);
		else if (f->is_quad()) std::cout << " is quadrature";
		else std::cout << " is polygon";
		std::cout << std::endl;
	};
};

void step3_triangle(){
	using Kernel=CGAL::Simple_cartesian<double>;
	using Triangulation=CGAL::Triangulation_3<Kernel>;
	using Point=CGAL::Point_3<Kernel>;
	double xf[3] {0.,0.,0.};
	double w[3] {1.,1.,1.};
	auto Q=make_cube(xf,w);
	//	print_vertex(Q);
	//	print_facet(Q);
	//	std::cout << std::endl;
	//三角分解
	std::cout << "三角分解" << std::endl;
	std::vector<Point> points;
	for(auto it=Q.vertices_begin();it!=Q.vertices_end();it++) points.push_back(it->point());
	Triangulation Tri(points.begin(),points.end());
	std::cout << "Points= " << Tri.number_of_vertices() << " cells= " << Tri.number_of_cells()
	<< " edges= " << Tri.number_of_edges() << " facets= " <<  Tri.number_of_facets() << std::endl;
	std::cout << "Finite Cells= " << Tri.number_of_finite_cells()
	<< " Finite Edges= " << Tri.number_of_finite_edges()
	<< " Finite Facets= " << Tri.number_of_finite_facets() << std::endl;
	//セルをリストしてみる
	int n=0;
	for(auto it=Tri.finite_cells_begin();it!=Tri.finite_cells_end();it++) {
		std::cout << "Cell #" << ++n;
		std::cout << "(" << it->vertex(0)->point() << ")(" << it->vertex(1)->point() << ")(" << it->vertex(2)->point()
		<< ")(" << it->vertex(3)->point() << ") with volume= " << Tri.tetrahedron(it).volume() << std::endl;
	}
	//		for (auto v = Tra.vertices_begin(); v != Tra.vertices_end(); ++v) std::cout << "Vertex " << v->point() << std::endl;
	//		std::cout << "Cell #" << n  << " vertices= " << Tra.vertex(0) << " " << Tra.vertex(1) << " " << Tra.vertex(2) << std::endl;
	
	// writing file output;
	//	std::ofstream oFileT("output",std::ios::out);
	//	oFileT << Tri;
	//(x,y,z)座標から, セルを取得する
	//	Triangulation::Locate_type Type;
	//	int li,lj;
	//	Point here(0,3,0);
	//	Triangulation::Cell_handle CH=Tri.locate(here,Type,li,lj);//場所を指定してType, (li,lj)を取得
	//	switch (Type) {
	//		case Triangulation::VERTEX:
	//			std::cout << " You (" << here << ") are on the vertex #= " << li << std::endl;break;
	//		case Triangulation::EDGE:
	//			std::cout << " You (" << here << ") are on the edge between= " << li << " and " << lj << std::endl;break;
	//		case Triangulation::FACET:
	//			std::cout << " You (" << here << ") are on the facet #= " << li << std::endl;break;
	//		case Triangulation::CELL:
	//			std::cout << " You (" << here << ") are in the cell index= " << li << ":" << lj << std::endl;	break;
	//		case Triangulation::OUTSIDE_AFFINE_HULL:
	//			std::cout << " You (" << here << ") are outsider" << std::endl;break;
	//		case Triangulation::OUTSIDE_CONVEX_HULL:
	//			std::cout << " You (" << here << ") are outsider" << std::endl;break;
	//	}
	
	//	for(auto it=Tri.cells_begin();it!=Tri.cells_end();it++) {
	//		CGAL::Triangulation_3<Kernel>::Cell C=*it;
	//		std::cout << << std::endl;
	//	}
}
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
void print_vertex(const CGAL::Polyhedron_3<CGAL::Exact_predicates_exact_constructions_kernel>& P){
	int n=0;
	for (auto v = P.vertices_begin(); v != P.vertices_end(); ++v) std::cout << "Vertex " << ++n << " = " << v->point() << std::endl;
};
void print_facet(const CGAL::Polyhedron_3<CGAL::Exact_predicates_exact_constructions_kernel>& P){
	using Kernel=CGAL::Exact_predicates_exact_constructions_kernel;
	auto triangle_normal=[](const CGAL::Polyhedron_3<Kernel>::Facet& f){
		return Kernel::Construct_normal_3()(f.halfedge()->vertex()->point(),f.halfedge()->next()->vertex()->point(),f.halfedge()->opposite()->vertex()->point() );
	};
	//	auto triangle_area=[](const CGAL::Polyhedron_3<Kernel>::Facet& f){
	//		return Kernel::Compute_area_3()(f.halfedge()->vertex()->point(),f.halfedge()->next()->vertex()->point(),f.halfedge()->opposite()->vertex()->point() );
	//	};
	int n=0;
	for (auto f = P.facets_begin(); f != P.facets_end(); ++f) {
		n++;
		std::cout << "Facet " << n;
		if (f->is_triangle()) std::cout << " is triangle area normal to " << triangle_normal(*f);
		// << " with area= " << triangle_area(*f);
		else if (f->is_quad()) std::cout << " is quadrature";
		else std::cout << " is polygon";
		std::cout << std::endl;
	};
};
void step4_nef_geometry(){
	//	using Kernel=CGAL::Extended_homogeneous<CGAL::Exact_integer>;
	//	Nef_polyhedron N0;
	//	Nef_polyhedron N1(Nef_polyhedron::EMPTY);		//空集合
	//	Nef_polyhedron N2(Nef_polyhedron::COMPLETE);	//全集合
	//	std::cout << "N1はN2の補集合ですか? " << (N1 == N2.complement()) << "では, N1はN1の補集合ですか? " << (N1 == N1.complement()) << std::endl;
	//整数カーネルCGAL::Extended_homogeneous<CGAL::Exact_integer>では, 集合の包含関係も計算できる
	//	Nef_polyhedron N3(Plane_3( 1.0, 2.0, 5.0,-1.0));									//この生成法は整数の場合に限る
	//	Nef_polyhedron N4(Plane_3( 1.0, 2.0, 5.0,-1.0), Nef_polyhedron::INCLUDED);	//N3=N4である
	//	Nef_polyhedron N5(Plane_3( 1.0, 2.0, 5.0,-1.0), Nef_polyhedron::EXCLUDED);
	//	std::cout << "N3はN4ですか? " << (N3 == N4) << " では, N3はN5ですか? " << (N3==N5) << std::endl;
	//	std::cout << "N5はN4の部分集合ですね: " << (N5<N4) << std::endl;
	using Kernel=CGAL::Exact_predicates_exact_constructions_kernel;
	using Nef_polyhedron=CGAL::Nef_polyhedron_3<Kernel>;
	using Plane_3=Nef_polyhedron::Plane_3;
	//三角錐を作る
	Kernel::Point_3 p( 1.0, 0.0, 0.0);
	Kernel::Point_3 q( 0.0, 0.0, 1.0);
	Kernel::Point_3 r( 0.0, 0.0, 0.0);
	Kernel::Point_3 s( 0.0, 1.0, 0.0);
	CGAL::Polyhedron_3<Kernel> P;
	auto halfedge=P.make_tetrahedron( p, q, r, s);
	print_vertex(P);
	print_facet(P);
	if (P.is_closed()) {
		//Polyhedron -> Nef_polyhedron
		Nef_polyhedron N(P);
		if(N.is_simple()) {
			//Nef_polyhedron -> Polyhedron
			N.convert_to_polyhedron(P);
			print_vertex(P);
			print_facet(P);
			std::ofstream ofs("output.off");
			ofs << P;
		}
	}
}
void print_vertex(const CGAL::Tetrahedron_3<CGAL::Exact_predicates_exact_constructions_kernel>& P){
	int n=0;
	for(int i=0;i<4;i++) std::cout << "Vertex " << i << " = " << P.vertex(i) << std::endl;
};
void step5_affine(){
	using Kernel=CGAL::Exact_predicates_exact_constructions_kernel;
	//三角錐を作る
	Kernel::Point_3 p( 1.0, 0.0, 0.0);
	Kernel::Point_3 q( 0.0, 0.0, 1.0);
	Kernel::Point_3 r( 0.0, 0.0, 0.0);
	Kernel::Point_3 s( 0.0, 1.0, 0.0);
	CGAL::Tetrahedron_3<Kernel> T(p,q,r,s);
	print_vertex(T);
	auto T2=T;
	{
		std::cout << "Move (0.5,0.1,0.3)" << std::endl;
		CGAL::Aff_transformation_3<Kernel> Mat(
											   1.0,	0.0,	0.0,	0.5,
											   0.0,	1.0,	0.0,	0.1,
											   0.0,	0.0,	1.0,	0.3);
		T2=T.transform(Mat);
		print_vertex(T2);
	}
	{
		std::cout << "rotate (45,0,0)" << std::endl;
		double tx=M_PI/4,ty=0.0,tz=0.0;
		CGAL::Aff_transformation_3<Kernel> Rx(
											  1.0, 0.0     ,	     0.0,	0.,
											  0.0, cos(tx),-sin(tx),	0.,
											  0.0,	 sin(tx),+cos(tx),	0.);
		T2=T.transform(Rx);
		print_vertex(T2);
		
	}
}
int main(int argc, const char * argv[]) {
	step4_nef_geometry();
	return 0;
}

