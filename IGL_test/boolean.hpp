//
//  boolean.hpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/30.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//

#ifndef boolean_hpp
#define boolean_hpp

#include <iostream>
#include <vector>
#include <Eigen/Dense>

#undef USE_CGAL

//#ifdef USE_CGAL
//#define IGL_STATIC_LIBRARY
//#include <igl/copyleft/cgal/mesh_boolean.h>
//#else
//#include <igl/copyleft/cork/mesh_boolean.h>
//#endif
//
//void make_union(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
//void make_intersect(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
//void make_minus(Eigen::MatrixXd VA,Eigen::MatrixXi FA,Eigen::MatrixXd VB,Eigen::MatrixXi FB,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
//void make_xor(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
//
//

#endif /* boolean_hpp */
