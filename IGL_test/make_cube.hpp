//
//  make_cube.hpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/29.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//

#ifndef make_cube_hpp
#define make_cube_hpp
#include <iostream>
#include <vector>
#include <Eigen/Dense>

void unit_tetrahedron(Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void unit_cube(Eigen::MatrixXd& V,Eigen::MatrixXi& F);
//void make_affine(Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void make_affine(Eigen::MatrixXd& V,Eigen::MatrixXi& F,double (&fr)[3],double (&w)[3],int normal,double (&v)[3]);
void make_shift(Eigen::MatrixXd& V,Eigen::MatrixXi& F,double (&fr)[3]);


#endif /* make_cube_hpp */
