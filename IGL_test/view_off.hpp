//
//  view_off.hpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/29.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//

#ifndef view_off_hpp
#define view_off_hpp
#include <map>
#define TUTORIAL_SHARED_PATH "/usr/local/share/igl/data"
#include <igl/readOFF.h>
#include <igl/writeOFF.h>
#include <igl/writeOBJ.h>
#include <igl/opengl/glfw/Viewer.h>
void view_off();
void load_off(std::string fname,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void save_obj(std::string fname,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void save_off(std::string fname,Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void load_matrix(Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void view_matrix(Eigen::MatrixXd& V,Eigen::MatrixXi& F);
void view_matrices(std::vector<std::pair<Eigen::MatrixXd,Eigen::MatrixXi>> List);


#endif /* view_off_hpp */
