//
//  boolean.cpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/30.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//

#include "boolean.hpp"

//void make_x(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,
//			Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,
//			Eigen::MatrixXd& V,Eigen::MatrixXi& F,int x){
//	igl::MeshBooleanType boolean_type=static_cast<igl::MeshBooleanType>(x);
//#ifdef USE_CGAL
//	igl::copyleft::cgal::mesh_boolean(VA,FA,VB,FB,boolean_type,V,F);
//#else
//	igl::copyleft::cork::mesh_boolean(VA,FA,VB,FB,boolean_type,V,F);
//#endif
//}

//void make_union(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,
//				Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,
//				Eigen::MatrixXd& V,Eigen::MatrixXi& F){
//	make_x(VA,FA,VB,FB,V,F,0);
//};
//void make_intersect(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,
//					Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,
//					Eigen::MatrixXd& V,Eigen::MatrixXi& F){
//	make_x(VA,FA,VB,FB,V,F,1);
//};
//void make_minus(Eigen::MatrixXd VA,Eigen::MatrixXi FA,
//				Eigen::MatrixXd VB,Eigen::MatrixXi FB,
//				Eigen::MatrixXd& V,Eigen::MatrixXi& F){
//	make_x(VA,FA,VB,FB,V,F,2);
//};
//void make_xor(Eigen::MatrixXd& VA,Eigen::MatrixXi& FA,
//			  Eigen::MatrixXd& VB,Eigen::MatrixXi& FB,
//			  Eigen::MatrixXd& V,Eigen::MatrixXi& F){
//	make_x(VA,FA,VB,FB,V,F,3);
//};
