//
//  view_off.cpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/29.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//
#include "view_off.hpp"
//
//
//
void load_off(std::string fname,Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	igl::readOFF(fname,V,F);
}
void save_obj(std::string fname,Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	igl::writeOBJ(fname,V,F);
}
void save_off(std::string fname,Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	igl::writeOFF(fname,V,F);
}
void view_off(){
	Eigen::MatrixXd V;
	Eigen::MatrixXi F;
	// Load a mesh in OFF format
	igl::readOFF(TUTORIAL_SHARED_PATH "/bunny.off", V, F);
//	// Plot the mesh
	igl::opengl::glfw::Viewer viewer;
	viewer.data().set_mesh(V, F);
	viewer.launch();
}
void load_matrix(Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	igl::readOFF(TUTORIAL_SHARED_PATH "/bunny.off", V, F);
};
bool key_down(igl::opengl::glfw::Viewer& viewer, unsigned char key, int modifier)
{
//	std::cout<<"Key: "<<key<<" "<<(unsigned int)key<<std::endl;
	if (key == '1') viewer.core().camera_zoom+=0.05;
	else if (key == '2') viewer.core().camera_zoom-=0.05;
	return false;
}
void view_matrix(Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	igl::opengl::glfw::Viewer viewer;
	viewer.callback_key_down = &key_down;
	viewer.data().set_mesh(V, F);
	viewer.launch();
}
void view_matrices(std::vector<std::pair<Eigen::MatrixXd,Eigen::MatrixXi>> List){
	igl::opengl::glfw::Viewer viewer;
	viewer.callback_key_down = &key_down;
	std::map<int, Eigen::RowVector3d> colors;
	int count=0;
	for(auto &pair:List) {
		auto &V=pair.first;
		auto &F=pair.second;
		viewer.data().set_mesh(V, F);
		viewer.data().id=count++;
		viewer.data_list.push_back(viewer.data());
		colors.emplace(viewer.data().id, Eigen::RowVector3d::Random().array()*0.5+0.5);
	}
	for (auto &data : viewer.data_list) data.set_face_based(true);
	int last_selected = -1;
	viewer.callback_key_down =[&](igl::opengl::glfw::Viewer &, unsigned int key, int mod){
		if(key == GLFW_KEY_BACKSPACE) {
			int old_id = viewer.data().id;
			if (viewer.erase_mesh(viewer.selected_data_index)) {
				colors.erase(old_id);
				last_selected = -1;
			}
			return true;
		} else if (key == '1') viewer.core().camera_zoom+=0.05;
		else if (key == '2') viewer.core().camera_zoom-=0.05;
		return false;
	};
	viewer.callback_pre_draw =[&](igl::opengl::glfw::Viewer &){
		if (last_selected != viewer.selected_data_index) {
			for (auto &data : viewer.data_list) data.set_colors(colors[data.id]);
			viewer.data_list[viewer.selected_data_index].set_colors(Eigen::RowVector3d(0.95,0.95,0.95));
			last_selected = viewer.selected_data_index;
			std::cout << "Selected " << last_selected << std::endl;
		}
		return false;
	};
	viewer.launch();
}

