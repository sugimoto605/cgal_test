//
//  main.cpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/29.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//
#include <iostream>
#include <boost/filesystem.hpp>
#include <Eigen/Dense>
//#include "open_gl.hpp"
#include "view_off.hpp"
#include "make_cube.hpp"
#include "boolean.hpp"
#include "make_tetra.hpp"

//	open_gl_test(argc,argv);	ネットからダウンロードしたOpenGLのテストプログラム
int Usagi(){
	view_off();//	うさぎを描く
}
//int Usagi2(){
//	Eigen::MatrixXd V;
//	Eigen::MatrixXi F;
//	load_matrix(V,F);	//	うさぎをファイルからロードする
//	view_matrix(V,F);	//	うさぎを描く
//}
//int Usagi3(){
//	Eigen::MatrixXd V;
//	Eigen::MatrixXi F;
//	unit_cube(V,F);
//	std::cout << "Column " << V.rows() << " x " << V.cols() << std::endl;
//	for(int i=0;i<V.rows();i++) {
//		auto x=V.row(i);
//		std::cout << x << std::endl;
//	}
//	view_matrix(V,F);
//}
//int Usagi4(){
//	std::vector<std::pair<Eigen::MatrixXd,Eigen::MatrixXi>> DATA;
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		unit_cube(V,F);
//		DATA.emplace_back(std::make_pair(V,F));
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		double from[3] {1.,0.,0.};
//		double width[3] {0.1,0.1,0.1};
//		int normal=0;
//		double v[3] {2.5,1.5,0.5};
//		make_affine(V,F,from,width,normal,v);
//		DATA.emplace_back(std::make_pair(V,F));
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		double from[3] {0.,1.,0.};
//		double width[3] {0.1,0.1,0.1};
//		int normal=0;
//		double v[3] {2.5,1.5,0.5};
//		make_affine(V,F,from,width,normal,v);
//		DATA.emplace_back(std::make_pair(V,F));
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		double from[3] {-0.5,-0.5,-0.2};
//		double width[3] {0.1,0.5,0.5};
//		int normal=1;
//		double v[3] {5,3,1};
//		make_affine(V,F,from,width,normal,v);
//		DATA.emplace_back(std::make_pair(V,F));
//	}
//	view_matrices(DATA);
////	view_matrix(V,F);
//}
//void Usagi5(){
//	boost::filesystem::path basedir=std::getenv("HOME");
//	basedir/="tmp";
//	std::vector<std::pair<Eigen::MatrixXd,Eigen::MatrixXi>> DATA;
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		unit_cube(V,F);
//		boost::filesystem::path file=basedir/"cube.off";
//		save_off(file.string(),V,F);
//		DATA.push_back(std::make_pair(V,F));
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		double from[3] {-0.5,-0.5,-0.2};
//		double width[3] {0.1,0.5,0.5};
//		int normal=1;
//		double v[3] {5,3,1};
//		make_affine(V,F,from,width,normal,v);
//		boost::filesystem::path file=basedir/"parallelepiped.off";
//		save_off(file.string(),V,F);
//		DATA.push_back(std::make_pair(V,F));
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		make_minus(DATA[1].first,DATA[1].second,DATA[0].first,DATA[0].second,V,F);
//		boost::filesystem::path file=basedir/"minus.off";
//		save_off(file.string(),V,F);
//		DATA.push_back(std::make_pair(V,F));
//		view_matrix(V,F);
//	}
//}
//void Usagi6(){
//	Eigen::MatrixXd V;
//	Eigen::MatrixXi F;
//	unit_cube(V,F);
//	Eigen::MatrixXd TV;	//vertices
//	Eigen::MatrixXi TF;//facets
//	Eigen::MatrixXi TT;//tetrahedras
//	make_tetra(V,F,TV,TT,TF);
//	std::cout << "Triangulation vertices:" << std::endl;
//	for (int i=0; i<TV.rows();i++) {
//		std::cout << i << " : " << TV.row(i) << std::endl;
//	}
//	std::cout << "Triangulation tetrahedras:" << std::endl;
//	for (int i=0; i<TT.rows();i++) {
//		std::cout << i << " : " << TT.row(i) << std::endl;
//	}
//	std::cout << "Triangulation facets:" << std::endl;
//	for (int i=0; i<TF.rows();i++) {
//		std::cout << i << " : " << TF.row(i) << std::endl;
//	}
//	//OFFに変換
//	for (int i=0; i<TT.rows();i++) {
//		Eigen::MatrixXd V2;
//		Eigen::MatrixXi F2;
//		V2.resize(4,3);
//		F2.resize(4,3);
//		for(int k=0;k<4;k++) V2.row(k)=TV.row(TT(i,k));
//		F2(0,0)=0;		F2(0,1)=1;		F2(0,2)=3;
//		F2(1,0)=0;		F2(1,1)=2;		F2(1,2)=1;
//		F2(2,0)=3;		F2(2,1)=2;		F2(2,2)=0;
//		F2(3,0)=1;		F2(3,1)=2;		F2(3,2)=3;
//		std::ostringstream oss;
//		oss << "tra" << i << ".off";
//		save_off(oss.str(), V2, F2);
//	}
//}
//void Usagi7(){
//	Eigen::MatrixXd V_cube;
//	Eigen::MatrixXi F_cube;
//	unit_cube(V_cube,F_cube);
//	Eigen::MatrixXd V_palla;
//	Eigen::MatrixXi F_palla;
//	double from[3] {-0.5,-0.5,-0.2};
//	double width[3] {0.1,0.5,0.5};
//	int normal=1;
//	double v[3] {5,3,1};
//	make_affine(V_palla,F_palla,from,width,normal,v);
//	Eigen::MatrixXd V;
//	Eigen::MatrixXi F;
////	make_minus(V_cube,F_cube,V_palla,F_palla,V,F);
//	make_minus(V_palla,F_palla,V_cube,F_cube,V,F);
//	Eigen::MatrixXd TV;	//vertices
//	Eigen::MatrixXi TF;//facets
//	Eigen::MatrixXi TT;//tetrahedras
//	make_tetra(V,F,TV,TT,TF);
//	boost::filesystem::path basedir=std::getenv("HOME");
//	basedir/="tmp";
//	for (int i=0; i<TT.rows();i++) {
//		Eigen::MatrixXd V2;
//		Eigen::MatrixXi F2;
//		V2.resize(4,3);
//		F2.resize(4,3);
//		for(int k=0;k<4;k++) V2.row(k)=TV.row(TT(i,k));
//		F2(0,0)=0;		F2(0,1)=1;		F2(0,2)=3;
//		F2(1,0)=0;		F2(1,1)=2;		F2(1,2)=1;
//		F2(2,0)=3;		F2(2,1)=2;		F2(2,2)=0;
//		F2(3,0)=1;		F2(3,1)=2;		F2(3,2)=3;
//		std::ostringstream oss;
//		oss << "BX_" << i << ".off";
//		auto path=basedir/oss.str();
//		save_off(path.string(), V2, F2);
//	}
//	//体積
//	std::cout << "Cube Volume= " << volume(V_cube, F_cube) <<std::endl;
//	std::cout << "Pall Volume= " << volume(V_palla, F_palla) <<std::endl;
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		make_minus(V_cube,F_cube,V_palla,F_palla,V,F);
//		std::cout << "C-P = " << volume(V, F) <<std::endl;
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		make_minus(V_palla,F_palla,V_cube,F_cube,V,F);
//		std::cout << "P-C = " << volume(V, F) <<std::endl;
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		make_intersect(V_palla,F_palla,V_cube,F_cube,V,F);
//		std::cout << "P && C = " << volume(V, F) <<std::endl;
//	}
//	{
//		Eigen::MatrixXd V;
//		Eigen::MatrixXi F;
//		make_union(V_palla,F_palla,V_cube,F_cube,V,F);
//		std::cout << "P ^ C = " << volume(V, F) <<std::endl;
//	}
//}
int main(int argc,  char * argv[]) {
//	Usagi7();
	Usagi();
	return 0;
}
