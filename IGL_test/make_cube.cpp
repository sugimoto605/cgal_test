//
//  make_cube.cpp
//  IGL_test
//
//  Created by Hiroshi Sugimoto on 2019/06/29.
//  Copyright © 2019 Hiroshi Sugimoto. All rights reserved.
//

#include "make_cube.hpp"
void make_affine(Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	Eigen::MatrixXd V_u;
	unit_cube(V_u,F);
	V=V_u;
	Eigen::Affine3d SC(Eigen::Scaling(0.1,0.5,2.0));
	for(int i=0;i<V_u.rows();i++) V.row(i)=SC.linear()*V.row(i).transpose();
}
void make_shift(Eigen::MatrixXd& V,Eigen::MatrixXi& F,double (&fr)[3]){
	Eigen::MatrixXd V_u=V;
	Eigen::Affine3d SC(Eigen::Scaling(1.,1.,1.));
	SC.translation()=Eigen::Vector3d(fr[0],fr[1],fr[2]).transpose();
	for(int i=0;i<V_u.rows();i++) V.row(i)=SC*Eigen::Vector3d(V_u.row(i));
}
void make_affine(Eigen::MatrixXd& V,Eigen::MatrixXi& F,double (&fr)[3],double (&w)[3],int normal,double (&v)[3]){
	Eigen::MatrixXd V_u;
	unit_cube(V_u,F);
	Eigen::Affine3d SC(Eigen::Scaling(w[0],w[1],w[2]));
	SC.translation()=Eigen::Vector3d(fr[0],fr[1],fr[2]).transpose();
	Eigen::Vector3d vv(v[0],v[1],v[2]);
	switch (normal) {
		case 1:SC.linear().col(0)=vv;break;
		case 3:SC.linear().col(1)=vv;break;
		case 9:SC.linear().col(2)=vv;break;
	}
	V=V_u;
	for(int i=0;i<V_u.rows();i++) V.row(i)=SC*Eigen::Vector3d(V_u.row(i));
}
void unit_tetrahedron(Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	int number_of_vertices=4;
	int number_of_faces=4;
	V.resize(number_of_vertices,3);
	V(0,0)=0.;		V(0,1)=0.;		V(0,2)=0.;	//0
	V(1,0)=1.;		V(1,1)=0.;		V(1,2)=0.;	//1
	V(2,0)=0.;		V(2,1)=1.;		V(2,2)=0.;	//2
	V(3,0)=0.;		V(3,1)=0.;		V(3,2)=1.;	//3
	F.resize(number_of_faces,3);
	F(0,0)=0;		F(0,1)=1;		F(0,2)=3;
	F(1,0)=1;		F(1,1)=0;		F(1,2)=2;
	F(2,0)=2;		F(2,1)=0;		F(2,2)=3;
	F(3,0)=1;		F(3,1)=2;		F(3,2)=3;
}
void unit_cube(Eigen::MatrixXd& V,Eigen::MatrixXi& F){
	int number_of_vertices=8;
	int number_of_faces=12;
	V.resize(number_of_vertices,3);
	V(0,0)=0.;		V(0,1)=0.;		V(0,2)=0.;	//0
	V(1,0)=0.;		V(1,1)=0.;		V(1,2)=1.;	//1
	V(2,0)=0.;		V(2,1)=1.;		V(2,2)=0.;	//2
	V(3,0)=0.;		V(3,1)=1.;		V(3,2)=1.;	//3
	V(4,0)=1.;		V(4,1)=0.;		V(4,2)=0.;	//4
	V(5,0)=1.;		V(5,1)=0.;		V(5,2)=1.;	//5
	V(6,0)=1.;		V(6,1)=1.;		V(6,2)=0.;	//6
	V(7,0)=1.;		V(7,1)=1.;		V(7,2)=1.;	//7
	F.resize(number_of_faces,3);
	F(0,0)=0;		F(0,1)=6;		F(0,2)=4;
	F(1,0)=0;		F(1,1)=2;		F(1,2)=6;
	F(2,0)=0;		F(2,1)=3;		F(2,2)=2;
	F(3,0)=0;		F(3,1)=1;		F(3,2)=3;
	F(4,0)=2;		F(4,1)=7;		F(4,2)=6;
	F(5,0)=2;		F(5,1)=3;		F(5,2)=7;
	F(6,0)=4;		F(6,1)=6;		F(6,2)=7;
	F(7,0)=4;		F(7,1)=7;		F(7,2)=5;
	F(8,0)=0;		F(8,1)=4;		F(8,2)=5;
	F(9,0)=0;		F(9,1)=5;		F(9,2)=1;
	F(10,0)=1;		F(10,1)=5;		F(10,2)=7;
	F(11,0)=1;		F(11,1)=7;		F(11,2)=3;
}
