//
//  main.cpp
//  CGAL_mesh
//		Mesh_3/mesh_implicit_sphere.cpp
//
#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Labeled_mesh_domain_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/IO/output_to_vtu.h>


// Domain
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT FT;
typedef K::Point_3 Point;
typedef FT (Function)(const Point&);
typedef CGAL::Labeled_mesh_domain_3<K> Mesh_domain;

typedef CGAL::Sequential_tag Concurrency_tag;

// Triangulation
typedef CGAL::Mesh_triangulation_3<Mesh_domain,CGAL::Default,Concurrency_tag>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
// To avoid verbose function and named parameters call
using namespace CGAL::parameters;
// Function
FT sphere_function (const Point& p)
{ return CGAL::squared_distance(p, Point(CGAL::ORIGIN))-1; }

int main(int argc, const char * argv[]) {
	boost::filesystem::path cpwd=getenv("HOME");
	cpwd/="owncloud/FD/fd1/fig/paraview";
	boost::filesystem::current_path(cpwd);
	Mesh_domain domain = Mesh_domain::create_implicit_mesh_domain(sphere_function,K::Sphere_3(CGAL::ORIGIN, 2.));
	// Mesh criteria
//	Mesh_criteria criteria(facet_angle=30, facet_size=0.02, facet_distance=0.025,cell_radius_edge_ratio=2, cell_size=0.8);
//	cpwd/="sp_30_002_0025_2_08.vtu";
	Mesh_criteria criteria(facet_angle=30, facet_size=0.02, facet_distance=0.025,cell_radius_edge_ratio=2, cell_size=0.05);
	cpwd/="sp_30_002_0025_2_005.vtu";
	// Mesh generation
	C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria);
	// Output VTK unstructured grid
	std::ofstream file(cpwd.string());
	CGAL::output_to_vtu(file,c3t3);
	std::cout << cpwd.string() << std::endl;
	return 0;
}
